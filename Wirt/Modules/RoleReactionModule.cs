using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.Wirt.Data;
using Discord.Wirt.Models;
using Discord.Wirt.Services;
using Microsoft.Extensions.Logging;

namespace Discord.Wirt.Modules
{
    public class RoleReactionModule : ModuleBase<SocketCommandContext>
    {
        private readonly ILogger<RoleReactionModule> _logger;
        private readonly DialogService _dialog;
        private readonly DataContext _context;

        public RoleReactionModule(ILogger<RoleReactionModule> logger, DialogService dialog, DataContext context)
        {
            _logger = logger;
            _dialog = dialog;
            _context = context;
        }

        [Command("rolereaction")]
        public async Task MathAsync()
        {
            await Context.Message.DeleteAsync();

            var message =
                await ReplyAsync(
                    "Please send a Emoji and the @Role as a Message.\nTo finalize the Message, click on the reaction.");

            _dialog.NewDialog(message.Id, ExpectedResponseType.Message | ExpectedResponseType.Reaction);
            _dialog.OnResponse -= OnResponse;
            _dialog.OnResponse += OnResponse;
            _dialog.OnReactionAdded -= OnReactionAdded;
            _dialog.OnReactionAdded += OnReactionAdded;

            await message.AddReactionAsync(new Emoji("\U00002714"));
            await message.AddReactionAsync(new Emoji("\U0000274C"));
        }

        private async void OnResponse(object sender, ResponseEventArgs e)
        {
            _dialog.AddData(e.dialog.MessageID, e.message.Content);
            await e.message.DeleteAsync();
        }

        private async void OnReactionAdded(object sender, ReactionEventArgs e)
        {
            if (e.message.Reactions[e.reaction.Emote].ReactionCount == 1) return;

            if (e.reaction.Emote.Equals(new Emoji("\U00002714")))
            {
                var data = _dialog.GetData(e.dialog.MessageID);
                var emojis = new string[data.Length];

                string message = "Click on the reaction to add or remove the role.\n";

                for (int i = 0; i < data.Length; i++)
                {
                    var row = data[i].Trim();
                    var seperator = row.IndexOf(' ');
                    emojis[i] = row.Substring(0, seperator);
                    var role = row.Substring(seperator + 1);
                    message += "\n" + emojis[i] + " -> " + role;
                }

                var reactionMessage = await ReplyAsync(message);

                for (int i = 0; i < data.Length; i++)
                {
                    var emote = new Emoji(emojis[i]);
                    await reactionMessage.AddReactionAsync(emote);
                }
            }
            else if (e.reaction.Emote.Equals(new Emoji("\U0000274C")))
            {
            }
            else
            {
                return;
            }

            await e.message.DeleteAsync();
        }
    }
}