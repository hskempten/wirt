using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Discord.Commands;
using Microsoft.Extensions.Logging;

namespace Discord.Wirt.Modules
{
    public class AppointmentModule : ModuleBase<SocketCommandContext>
    {
        private static List<DateTime> list = new();
        private readonly ILogger<ExampleModule> _logger;

        public AppointmentModule(ILogger<ExampleModule> logger)
            => _logger = logger;

        [Command("saufen")]
        public async Task MathAsync([Remainder] DateTime date)
        {
            if (list.Contains(date))
            {
                var message = await ReplyAsync("Es gibt bereits einen Sauf-Termin, eigenen trotzdem eintragen?");
                await message.AddReactionAsync(new Emoji("\U00002705"));
                await message.AddReactionAsync(new Emoji("\U0000274E"));
                
                return;
            }
            await ReplyAsync("!Achtung! neuer Sauftermin am " + date.ToString("dd.MM.yyyy HH:mm"));
            list.Add(date);
            _logger.LogInformation($"{Context.User.Username} executed the math command!");
        }
    }
}