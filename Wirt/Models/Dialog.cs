﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Discord.Wirt.Models
{
    [Flags]
    public enum ExpectedResponseType
    {
        None = 0,
        Message = 1 << 0,
        Reaction = 1 << 2
    }

    public class Dialog
    {
        [Key] public ulong MessageID { get; set; }

        public ExpectedResponseType ExpectedResponseType { get; set; }

        public int Step { get; set; }

        public string Data { get; set; }

        public Dialog()
        {
        }

        public Dialog(ulong messageId, ExpectedResponseType expectedResponseType)
        {
            MessageID = messageId;
            ExpectedResponseType = expectedResponseType;
        }
    }
}