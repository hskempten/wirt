﻿using System.ComponentModel.DataAnnotations;

namespace Discord.Wirt.Models
{
    public class ReactionMessage
    {
        [Key]
        public long MessageId { get; set; } 
    }
}