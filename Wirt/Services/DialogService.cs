﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Discord.WebSocket;
using Discord.Wirt.Data;
using Discord.Wirt.Models;
using Microsoft.Extensions.Logging;

namespace Discord.Wirt.Services
{
    public class ReactionEventArgs : ResponseEventArgs
    {
        public readonly SocketReaction reaction;

        public ReactionEventArgs(Dialog dialog, IUserMessage message, SocketReaction reaction) : base(dialog, message)
        {
            this.reaction = reaction;
        }
    }

    public class ResponseEventArgs : EventArgs
    {
        public readonly Dialog dialog;
        public readonly IUserMessage message;

        public ResponseEventArgs(Dialog dialog, IUserMessage message)
        {
            this.dialog = dialog;
            this.message = message;
        }
    }

    public class DialogService
    {
        public delegate void ReactionAddedHandler(object sender, ReactionEventArgs e);

        public event ReactionAddedHandler OnReactionAdded;

        public delegate void ResponseHandler(object sender, ResponseEventArgs e);

        public event ResponseHandler OnResponse;

        private readonly DataContext _context;
        private readonly ILogger<DialogService> _logger;

        public DialogService(DataContext context, ILogger<DialogService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public void NewDialog(ulong messageId, ExpectedResponseType expectedResponseType)
        {
            _context.Dialogs.Add(new(messageId, expectedResponseType));
            _context.SaveChanges();
        }

        public void NewReaction(IUserMessage message, SocketReaction reaction)
        {
            var dialog = _context.Dialogs.Find(message.Id);
            if (dialog == null || (dialog.ExpectedResponseType & ExpectedResponseType.Reaction) !=
                ExpectedResponseType.Reaction)
                return;

            OnReactionAdded?.Invoke(this, new(dialog, message, reaction));

            // if (Equals(reaction.Emote, new Emoji("\U00002705")))
            //     await message.ReplyAsync("Ok, eingetragen");
            // else
            //     await message.ReplyAsync("Ok, ignoriert");
            // await message.DeleteAsync();
        }

        public bool NewResponse(SocketUserMessage message)
        {
            if (message.ReferencedMessage == null) return false;

            var dialog = _context.Dialogs.Find(message.ReferencedMessage.Id);
            if (dialog == null || (dialog.ExpectedResponseType & ExpectedResponseType.Message) !=
                ExpectedResponseType.Message)
                return false;

            OnResponse?.Invoke(this, new(dialog, message));

            return true;
        }

        public void AddData(ulong id, string data)
        {
            var dialog = _context.Dialogs.Find(id);
            data = data.Replace(";", ":,:");
            if (dialog.Data == null)
                dialog.Data = data;
            else
                dialog.Data += ";" + data;
        }

        public string[] GetData(ulong id)
        {
            var dialog = _context.Dialogs.Find(id);
            string[] data;
            if (dialog.Data == null)
                data = new string[] {""};
            if (dialog.Data.Contains(';'))
                data = dialog.Data.Split(";");
            else
                data = new string[] {dialog.Data};

            for (int i = 0; i < data.Length; i++)
                data[i] = data[i].Replace(":,:", ";");
            return data;
        }
    }
}